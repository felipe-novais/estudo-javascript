//novo recurso do ES2015

const pessoa = {
    nome: 'Anna',
    idade: 5,
    endereco: {
        logradouro: 'Rua ABC',
        numero: 1000
    }
    
}

const {nome , idade } = pessoa
console.log(nome, idade)

const {nome: n, idade: i} = pessoa
console.log(n,i)

const { sobrenome, bemHumorada = true} = pessoa
console.log(sobrenome, bemHumorada)

const{ endereco: {logradouro: l, numero: nm, cep: c}} = pessoa
console.log(l, nm, c)